# Changelog

## Version 3.0.0

- Add common package for query.

## Version 2.0.7

- add BBMRIResult classes

## Version 2.0.6

- add query converter class

## Version 2.0.3

- fixed the RorMetaReg class generation
- fixed the missing error classes

## Version 2.0.0

- added the schema files
- using maven to generate the Java classes
