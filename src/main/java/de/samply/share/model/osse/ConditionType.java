/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.model.osse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConditionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConditionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}And"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Eq"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Like"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Geq"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Gt"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}IsNotNull"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}IsNull"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Leq"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Lt"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Neq"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Or"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}In"/&gt;
 *         &lt;element ref="{http://schema.samply.de/osse/Query}Between"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionType", propOrder = {
    "andOrEqOrLike"
})
@XmlSeeAlso({
    Where.class,
    Or.class,
    And.class
})
public class ConditionType implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElements({
        @XmlElement(name = "And", type = And.class),
        @XmlElement(name = "Eq", type = Eq.class),
        @XmlElement(name = "Like", type = Like.class),
        @XmlElement(name = "Geq", type = Geq.class),
        @XmlElement(name = "Gt", type = Gt.class),
        @XmlElement(name = "IsNotNull", type = IsNotNull.class),
        @XmlElement(name = "IsNull", type = IsNull.class),
        @XmlElement(name = "Leq", type = Leq.class),
        @XmlElement(name = "Lt", type = Lt.class),
        @XmlElement(name = "Neq", type = Neq.class),
        @XmlElement(name = "Or", type = Or.class),
        @XmlElement(name = "In", type = In.class),
        @XmlElement(name = "Between", type = Between.class)
    })
    protected List<Serializable> andOrEqOrLike;

    /**
     * Gets the value of the andOrEqOrLike property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the andOrEqOrLike property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAndOrEqOrLike().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link And }
     * {@link Eq }
     * {@link Like }
     * {@link Geq }
     * {@link Gt }
     * {@link IsNotNull }
     * {@link IsNull }
     * {@link Leq }
     * {@link Lt }
     * {@link Neq }
     * {@link Or }
     * {@link In }
     * {@link Between }
     * 
     * 
     */
    public List<Serializable> getAndOrEqOrLike() {
        if (andOrEqOrLike == null) {
            andOrEqOrLike = new ArrayList<Serializable>();
        }
        return this.andOrEqOrLike;
    }

}
